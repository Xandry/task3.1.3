jQuery(document).ready(init());
$.getScript('js/modal.js');

function init() {

    // init create tab
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/v1/roles",
        crossDomain: true,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "x-requested-with"
        },
        success: function (data) {
            $.each(data, function (index, value) {
                // init roles list on user creation tab
                $('.form-create select').append('<option value="' + value.id + '">' + value.name + '</option>');
                $('.form-create .btn .btn-success').attr('onclick', 'createFormAjax()')
            })
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });

    // init data tab
    $.ajax({
        method: "GET",
        url: "http://localhost:8080/v1/users",
        crossDomain: true,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "x-requested-with"
        },
        success: function (data) {

            $('.form-create').attr('action', '/v1/');
            $.each(data, function (index, value) {
                initUserRow(index, value)
            });
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    })
}


let initUserRow = function initUserRow(index, value) {
    let $template = $('#user_list_template').clone();

    $template.attr('id', 'user_' + value.id);
    $template.attr('hidden', false);
    $template.find('.user_id').text(value.id);
    $template.find('.user_email').text(value.email);
    value.roles.forEach(x => $template.find('.user_roles').append('<option class="text-uppercase">' + x.name + ' </option>'));

    $template.find('.modal-delete').attr('id', 'deleteUser' + value.id);
    $template.find('.btn-delete').attr('data-target', '#deleteUser' + value.id);
    $template.find('.open-delete-window').attr('onclick', 'initDeleteModal(' + value.id + ')');


    $template.find('.modal-edit').attr('id', 'editUser' + value.id);
    $template.find('.btn-edit').attr('data-target', '#editUser' + value.id);
    $template.find('.open-edit-window').attr('onclick', 'initEditModal(' + value.id + ')');

    $template.appendTo($('#users_list'));
};


let clearData = function clearData() {
    $('#users_list').children().not(':first').remove();
    $('.form-create .custom-select').children().remove();
};

let extractCreateData = function extractCreateData() {
    let $form = $('.form-create');
    let roles = $form.find('.custom-select').val();

    return {
        email: $form.find('.email-input').val(),
        password: $form.find('.password-input').val(),
        roles: roles
    };
};

let extractEditData = function extractEditData(id) {
    let $element = $('#user_' + id);

    let roles = $element.find('.edit_role_select').val();
    return {
        email: $element.find('.email-input').val(),
        id: $element.find('.id-input').val(),
        password: $element.find('.password-input').val(),
        roles: roles
    };
};

let createFormAjax = function createFormAjax() {
    let data = extractCreateData();

    $.ajax({
        method: "POST",
        url: "http://localhost:8080/v1/users/",
        contentType: 'application/json',
        data: JSON.stringify(data),
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "x-requested-with"
        },
        success: function () {
            clearData();
            init()
        },
        error: function (error) {
            console.log(error);
        }
    })

};

let deleteFormAjax = function deleteFormAjax(id) {
    // close modal window
    $('#deleteUser' + id).modal('hide');

    $.ajax({
        method: "DELETE",
        url: "http://localhost:8080/v1/users/" + id,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "x-requested-with"
        },
        success: function () {
            clearData();
            init()
        },
        error: function (error) {
            console.log(error);
        }
    })
};

let editFormAjax = function editFormAjax(id) {

    let data = extractEditData(id);

    // close modal window
    $('#editUser' + data.id).modal('hide')

    $.ajax({
        method: "PUT",
        data: JSON.stringify(data),
        // dataType: 'json',
        contentType: 'application/json',
        url: "http://localhost:8080/v1/users/" + data.id,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "x-requested-with"
        },
        success: function () {
            clearData();
            init()
        },
        error: function (error) {
            console.log(error);
        }
    })
};


