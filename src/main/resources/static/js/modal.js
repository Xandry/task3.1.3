
let initEditModal = function initEditModal(id) {

    $.ajax({
        method: "GET",
        url: "http://localhost:8080/v1/users/" + id,
        crossDomain: true,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "x-requested-with"
        },
        success: function (value) {
            let $modal = $('#editUser' + id);

            $modal.find('.modal__edit_form').attr('id', 'userEditForm' + value.id);
            $modal.find('.modal__edit_form').find('.id-input').attr('value', value.id);
            $modal.find('.modal__edit_form').find('.password-input').attr('value', value.password);
            $modal.find('.modal__edit_form').find('.email-input').attr('value', value.email);
            $modal.find('.modal__edit_label').append(' ' + value.email);
            $modal.find('.btn-edit-confirm').attr('onclick', 'editFormAjax(' + value.id + ')');

            $.ajax({
                method: "GET",
                url: "http://localhost:8080/v1/roles",
                crossDomain: true,
                headers: {
                    "accept": "application/json;odata=verbose",
                    "content-type": "application/json;odata=verbose",
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Headers": "x-requested-with"
                },
                success: function (data) {
                    $.each(data, function (index, value) {
                        $modal.find('.edit_role_select').append('<option value="' + value.id + '">' + value.name + '</option>');
                    })
                }
            });

        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });

};

let initDeleteModal = function initDeleteModal(id) {

    $.ajax({
        method: "GET",
        url: "http://localhost:8080/v1/users/" + id,
        crossDomain: true,
        headers: {
            "accept": "application/json;odata=verbose",
            "content-type": "application/json;odata=verbose",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "x-requested-with"
        },
        success: function (value) {
            let $modal = $('#deleteUser' + id);

            $modal.find('.modal__delete_form').attr('id', 'userEditForm' + value.id);
            $modal.find('.modal__delete_form').find('.id-input').attr('value', value.id);
            $modal.find('.modal__delete_form').find('.password-input').attr('value', value.password);
            $modal.find('.modal__delete_form').find('.email-input').attr('value', value.email);
            $modal.find('.modal__delete_label').append(' ' + value.email);
            $modal.find('.btn-delete-confirm').attr('onclick', 'deleteFormAjax(' + value.id + ')');
            value.roles.forEach(x => $modal.find('.user_present_roles').append('<option class="text-uppercase">' + x.name + ' </option>'));

        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });

};