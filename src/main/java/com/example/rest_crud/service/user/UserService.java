package com.example.rest_crud.service.user;

import com.example.rest_crud.model.User;

import java.util.List;

public interface UserService {

    User getUser(long id);

    List<User> getAllUsers();

    void deleteUser(long id);

    void deleteAll();

    void addUser(User user);

    void updateUser(User user);

}
