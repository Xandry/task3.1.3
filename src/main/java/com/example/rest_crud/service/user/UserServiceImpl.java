package com.example.rest_crud.service.user;

import com.example.rest_crud.dao.role.RoleDao;
import com.example.rest_crud.dao.user.UserDao;
import com.example.rest_crud.model.Role;
import com.example.rest_crud.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final RoleDao roleDao;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao, RoleDao roleDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.roleDao = roleDao;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User getUser(long id) {
        return userDao.getUser(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Transactional
    @Override
    public void deleteUser(long id) {
        roleDao.removeAllRolesOfUser(id);
        userDao.deleteUser(id);
    }

    @Transactional
    @Override
    public void deleteAll() {
        userDao.deleteAll();
    }

    @Transactional
    @Override
    public void addUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userDao.addUser(user);
        roleDao.addAllRolesOfUser(user);
    }

    @Transactional
    @Override
    public void updateUser(User user) {
        List<Long> lostRoles = new ArrayList<>();
        List<Long> gainedRoles = new ArrayList<>();

        List<Long> rolesOfUserOldVersion = roleDao.getAllRolesIdsOfUser(user.getId());
        List<Long> rolesOfUserNewVersion = user.getRoles().stream().map(Role::getId).collect(Collectors.toList());
        Set<Long> allRolesIds = new HashSet<>();
        allRolesIds.addAll(rolesOfUserNewVersion);
        allRolesIds.addAll(rolesOfUserOldVersion);

        for (Long roleId : allRolesIds) {
            if (rolesOfUserOldVersion.contains(roleId) && !rolesOfUserNewVersion.contains(roleId)) {
                // user lost this role
                lostRoles.add(roleId);
            } else if (!rolesOfUserOldVersion.contains(roleId) && rolesOfUserNewVersion.contains(roleId)) {
                // user gained this role
                gainedRoles.add(roleId);
            }
        }

        roleDao.addRolesToUser(user.getId(), gainedRoles);
        roleDao.removeRolesOfUser(user.getId(), lostRoles);

        String oldPassword = getPasswordByUserId(user.getId());
        if (!oldPassword.equals(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }

        userDao.updateUser(user);
    }

    private String getPasswordByUserId(Long id) {
        return userDao.getUserPassword(id);
    }

}
