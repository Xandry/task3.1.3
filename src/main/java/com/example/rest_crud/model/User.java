package com.example.rest_crud.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "user")

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(of = {"email", "password"})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    private String email;

    private String password;

    @JsonManagedReference
    @ManyToMany(mappedBy = "users",
            fetch = FetchType.EAGER,
            cascade = {CascadeType.MERGE}
    )
    private Set<Role> roles;

}
